//
//  ContentView.swift
//  JesseCard
//
//  Created by Jesse Frederick on 2/22/23.
//

import SwiftUI

struct ContentView: View {
    init() {
        for family in UIFont.familyNames {
             print(family)
             for names in UIFont.fontNames(forFamilyName: family){
             print("== \(names)")
             }
        }
    }
    
    var body: some View {
        ZStack {
            Color(red: 0.20, green: 0.60, blue: 0.86, opacity: 1.00)
                .edgesIgnoringSafeArea(.all)
            VStack {
                Image("JFred")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 150, height: 150)
                    .clipShape(RoundedRectangle(cornerRadius: 25))
                    .overlay(
                        RoundedRectangle(cornerRadius: 25).stroke(Color.white, lineWidth: 5))
                Text("Jesse Frederick")
                    .font(Font.custom("Lobster", size: 50))
                    .bold()
                    .foregroundColor(.white)
                Text("iOS Developer")
                    .foregroundColor(.white)
                    .font(Font.custom("Inconsolata-UltraExpanded", size: 30))
                Divider()
                InfoView(text: "+1 217-691-8622", imageName: "phone.fill")
                InfoView(text: "jessefred1997@gmail.com", imageName: "envelope.fill")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


