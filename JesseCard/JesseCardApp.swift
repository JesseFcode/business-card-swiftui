//
//  JesseCardApp.swift
//  JesseCard
//
//  Created by Jesse Frederick on 2/22/23.
//

import SwiftUI

@main
struct JesseCardApp: App {
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
